## Appwork - development version main change
## Appwork - development version

Welcome to Appwork's private repository. Here you can get the latest package updates.

The development version is designed in the way that let maximally avoid of repeating items.

## Set up environment

Before you start, you need to set up required components.

#### Windows

  1.  Download and install Nodejs 10.15.x LTS from [https://nodejs.org/en/](https://nodejs.org/en/).
  1.  Download and install Git from [https://git-scm.com/downloads](https://git-scm.com/downloads).
  1.  Download and install Yarn from [https://yarnpkg.com/en/docs/install](https://yarnpkg.com/en/docs/install).
  1.  Logout from the system and login again.
  1.  Launch console as administrator and run command `npm install --add-python-to-path='true' --global --production windows-build-tools`.
  1.  Logout from the system and login again.

#### Ubuntu
  1.  Upgrade system: `sudo apt-get update && sudo apt-get upgrade`.
  1.  Install curl: `sudo apt-get install curl`.
  1.  Install Nodejs 10.15.x LTS and build tools following instructions on [https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions-enterprise-linux-fedora-and-snap-packages](https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions-enterprise-linux-fedora-and-snap-packages).
  1.  Install Git following instructions on [https://git-scm.com/download/linux](https://git-scm.com/download/linux).
  1.  Install Yarn following instructions on [https://yarnpkg.com/en/docs/install#debian-stable](https://yarnpkg.com/en/docs/install#debian-stable).

#### Mac OS X

  1.  Install Xcode from App Store. After installing, launch the Xcode, accept the license agreement and wait while components installed.
  1.  Install Homebrew following instructions on [https://brew.sh](https://brew.sh).
  1.  Download and install Nodejs 10.15.x LTS from [https://nodejs.org/en/](https://nodejs.org/en/).
  1.  Download and install Git from [https://git-scm.com/downloads](https://git-scm.com/downloads).
  1.  Install Yarn following instructions on [https://yarnpkg.com/en/docs/install](https://yarnpkg.com/en/docs/install).

## Build

Now you can clone the repository, install NPM packages and build the project by executing only one script:

  1.  Clone the repository.
  1.  Open console/terminal and go to the `dev` directory.
  1.  
      *  On Windows: run `setup.bat`.
      *  On Ubuntu / Mac OS X: run `./setup.sh`. If you get a permissions error,
         grant execute permissions to the script: `sudo chmod 777 setup.sh`.

The `setup.bat`/`setup.sh` script will perform the next actions:

  1.  Install NPM packages in `dev` directory.
  1.  Install NPM packages in `src` directory.
  1.  Install NPM packages in `angular-demo` directory.
  1.  Install NPM packages in `vue-demo` directory.
  1.  Run `build` task.
  1.  Run `sort-classes` task.
  1.  Run `generate-starter` task.
  1.  Run `generate-demo` task.
  1.  Run `generate-docs` task.

## Available tasks

Please note that tasks in the `dev` directory are not the same as in the `src` directory - they performs some extra work. For example, Angular and Vue.js versions not contain Appwork's assets by default, it will be copied from the `src` directory and transformed by `build` tasks.

#### `build`

  1.  Clean up `buildPath` directory.
  1.  Run `build` task in the `src` directory.
  1.  Build assets and generate html files.
  1.  Prefix plain css with Autoprefixer.
  1.  Sort `class` attributes in the `*.html` and `*.vue` files.

#### `build:css`

Build stylesheets and libs.

#### `build:js`

Build javascrips.

#### `build:fonts`

Build fonts.

#### `build:copy`

Copy static files.

#### `prefix-css`

Prefix plain css with Autoprefixer.

#### `sort-classes`

Sort `class` attributes in the `*.html` and `*.vue` files.

#### `generate-starter`

Copy assets and some required files in starter projects.

#### `generate-demo`

Copy assets and some required files in demo projects.

#### `generate-docs`

Copy assets in `docs` directory and generate html files.

#### `watch`

Watch files for changes and automatically run build when changed.
